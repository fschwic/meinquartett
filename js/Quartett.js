function importTemplate(url) {
  console.log("Loading template " + url + ".");
  $.ajax({
    dataType: "html",
    url: url,
    async: false,
    success: function(data) {
      $('head').prepend(data);
      //console.log(data);
    }
  });
}

importTemplate('QuartettKaarte.tpl.html');

var quartettAblage = "Examples/";
var cards = {};

customElements.define('quartett-deck',
  class extends HTMLElement {
    constructor() {
      super();
    }

    connectedCallback() {

      var quartettName = this.getAttribute('name');
      var quartettPfad = quartettAblage + quartettName + "/";
      var quartett;

      // Quartett-Definition laden
      console.log("Initialisiere Quartett '" + quartettName + "'.");
      $.ajax({
        dataType: "json",
        url: quartettPfad + 'index.json',
        async: false,
        success: function(data) {
          quartett = data;
          //console.log(quartett);
        },
        error: function(q, msg, e) {
          console.log("Fehler beim Holen der Quartettdefinition: " + msg);
          console.log(e);
        }
      });

      function cardToBack(id) {
        $('#' + id).css('z-index', '0');
        $('quartett-karte').each(function(i, card) {
          var zindex = Number($(card).css('z-index'));
          $(card).css('z-idex', zindex + 1);
        });
      }

      function cardToFront() {
        var numOfCards = quartett.cards.length;
        $('quartett-karte').each(function(i, card) {
          var zindex = Number($(card).css('z-index'));
          if (zindex == 1) {
            $(card).css('z-index', numOfCards);
          } else {
            $(card).css('z-index', zindex - 1);
          }
        });
      }

      function shuffle() {
        var count = $('quartett-karte').length;
        var positions = [];
        for (var i = 0; i < count; i++) {
          var random = Math.floor(Math.random() * count) + 1;
          while (positions.includes(random)) {
            random = Math.floor(Math.random() * count) + 1;
          }
          positions.push(random);
        }

        $('quartett-karte').each(function(i, card) {
          $(card).css('z-index', positions[i]);
        });
      }

      // Erweitern der Vorlage um Felder des Quartetts
      let template = document.getElementById('template-quartett-karte');
      let templateFieldContainer = template.content.querySelectorAll('.kartendaten')[0];
      for (var i = 0; i < quartett.fields.length; i++) {
        var field = quartett.fields[i];

        var label = document.createElement('span');
        label.setAttribute('class', field.key + " label");
        label.append(field.label + ': ');

        var value = document.createElement('span');
        value.setAttribute('class', field.key + " value");
        if (field.unit) {
          value.setAttribute('data-unit', field.unit);
        }
        if (field.type) {
          value.setAttribute('data-type', field.type);
        }

        templateFieldContainer.appendChild(label);
        templateFieldContainer.appendChild(value);
      }

      // Erzeugen der Quartettkarten
      for (var i = 0; i < quartett.cards.length; i++) {
        var cardId = quartett.cards[i].id;
        cards[cardId] = quartett.cards[i];
        var card = document.createElement('quartett-karte');
        card.setAttribute('id', cardId);

        $(card).on('click', function(e) {
          cardToBack(e.target.getAttribute('id'));
        });

        var delayed;
        $(card).on('mousedown', function(e) {
          delayed = setTimeout(function() {
            cardToFront();
          }, 1000);
        });
        $(card).on('mouseup', function(e) {
          clearTimeout(delayed);
        });


        $(card).css('z-index', i + 1);
        $(card).css('position', "absolute");
        $(card).css('top', "0px");
        $(card).css('left', "0px");
        $(card).css('background', "url(" + quartettPfad + quartett.background + ")");
        document.getElementById('deck').appendChild(card);
      }

      shuffle();

    }
  });

customElements.define('quartett-karte',
  class extends HTMLElement {
    constructor() {
      super();

      let template = document.getElementById('template-quartett-karte');
      let templateContent = template.content;
      const shadow = this.attachShadow({
        mode: 'open'
      }).appendChild(templateContent.cloneNode(true));

    }

    connectedCallback() {
      var cardId = this.getAttribute('id');
      var card = cards[cardId];
      var shadow = this.shadowRoot;
      shadow.querySelectorAll('.kartentitel')[0].append(card.title);
      shadow.querySelectorAll('.kartenbild img')[0].src = quartettAblage + this.parentElement.getAttribute('name') + "/" + card.image;
      for (var key in card) {
        var fieldvalue = shadow.querySelectorAll('.value.' + key)[0];
        if (fieldvalue) {
          var type = fieldvalue.getAttribute('data-type');
          var value = card[key];
          if (type === "number") {
            var ca = false;
            if (value.startsWith('~')) {
              ca = true;
              value = value.substring(1);
            }
            var float = parseFloat(value);
            if (isNaN(float)) {
              value = "-";
            } else {
              if (float > 1e10) {
                value = float.toExponential(3).replace('.', ',').replace('e+', ' &bull;10<sup>') + '</sup>';
              } else {
                value = float.toLocaleString('de-DE');
              }
            }
            if (ca) {
              value = "~" + value;
            }
          }
          $(fieldvalue).append(value);
          var unit = fieldvalue.getAttribute('data-unit');
          if (unit) {
            $(fieldvalue).append(" " + unit);
          }
        }
      }
    }

  });
